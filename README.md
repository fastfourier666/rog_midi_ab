# Rog's MIDI AB

Arduino code & schematic for a clever-ish 2in 1out MIDI AB box

## Parts notes
* Microcontroller is an [ESP32 devkit clone](https://www.amazon.co.uk/AZDelivery-NodeMcu-CP2102-Development-including/dp/B074RGW2VQ/ref=sr_1_3?crid=1Q0PTDOO9XFQG&keywords=esp32&qid=1679067055&sprefix=esp32%2B%2Caps%2C191&sr=8-3&th=1)
* Optoisolators are H11L1 or Sharp PC900
* LED resistors value unknown as yet; will depennd on type of LEDs used
* can always use LEDC to dim leds if they're too bright

## Firmware/IDE install notes

Download the Arduino legacy IDE 1.8.19 from [this page](https://www.arduino.cc/en/software). The new IDE (2.x) will work as well, but I don't know how to use it yet.

Run IDE and install ESP32 support following the instructions on [this page](https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html)

Install the debouncing library: Tools -> Manage Libraries, search for "bounce2" and install

Download this source code and extract to ~/Documents/Arduino (full path to the .ino file should be `/Users/youruser/Documents/Arduino/rog_midi_ab/rog_midi_ab.ino`)

Restart the IDE and load the sketch via File -> Sketchbook

In the IDE, select the correct board: Tools -> Board -> ESP32 Arduino -> ESP32 Dev Module

With the development board plugged in to the computer, under Tools -> Port you should see something like `/dev/cu.SLAB_USBtoUART`. If not, you will need [this driver](https://www.silabs.com/documents/public/software/Mac_OSX_VCP_Driver.zip)

Compile and upload the sketch: cmd-U

## Operation

Two of the three ESP32 UARTS are used for MIDI I/O. UART 0 is left for serial debugging and firmware upload.

* UART1 is MIDI In A, output is not used
* UART2 is MIDI In B and MIDI Out

The firmware doesn't (currently) understand anything about the MIDI protocol, it just passes bytes from input to output.

When switching between inputs, a few things happen:

 1. the relevant LEDs are set on or off
 2. "panic" is sent to the output (128 * 16 note off messages)
 3. the receive buffer of the input we switched to is dumped

"Panic" takes about 2 seconds to send all the note off messages (16 ch * 128 notes * 3 bytes * 10 bits = 61440 bit times. 61440/31250 == 1.96608 seconds). IS THIS ACCEPTABLE? do we want a mode that can send CC 123 or whatever it is, instead, which will only take 16 messages?

Any data received on the "destination" port while the panic is in progress will be discarded. To do this, or not, is another behavioural question. We can flush, panic and then output everything we received during the panic, but the timing will be all wrong. We could also commence passing thru notes while sending the panic. not sure if this is a good idea though.

 Point 3 is where problems might arise. If we switch to the destination port while it's part way through receiving a message (lets say it already got a status byte), the subsequent data bytes will be transmitted. Hopefully this will not affect anything since sending all those note off messages will have stopped the downstream device from using running status. It will ignore the data bytes and not do anything until it has received a fresh data byte.

 If this doesn't work, then I'll have to write a slightly more cleverer version of this which understands the MIDI protocol a bit more, and can ditch everything up until the next status byte?

