#include <Bounce2.h>
#include <driver/uart.h>

#define PIN_MIDI_IN_A     39
#define PIN_MIDI_IN_B     36
#define PIN_MIDI_OUT      13
#define PIN_SELECT_SW     35
#define PIN_ACTIVITY_A    2
#define PIN_ACTIVITY_B    4
#define LED_BLINK_TIME    10      // milliseconds to wait until we turn the LED back on

#define MIDI_OUT          Serial2

uint8_t source=0;             // no source to begin with
uint32_t offTime=0;           // timeout for controlling LED activity blink
Bounce debouncer = Bounce(); // Instantiate a Bounce object


void setup() {
  // put your setup code here, to run once:
  Serial.begin (115200);
  Serial1.begin (31250, SERIAL_8N1, PIN_MIDI_IN_A, -1);
  Serial2.begin (31250, SERIAL_8N1, PIN_MIDI_IN_B, PIN_MIDI_OUT);

  debouncer.attach(PIN_SELECT_SW,INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
  debouncer.interval(25); // Use a debounce interval of 25 milliseconds

  source = debouncer.read() ? 2 : 1;            // set the initial source

  pinMode (PIN_ACTIVITY_A, OUTPUT);
  pinMode (PIN_ACTIVITY_B, OUTPUT);
  digitalWrite (PIN_ACTIVITY_A, LOW);
  digitalWrite (PIN_ACTIVITY_B, LOW);
}

void loop() {
  // handle changes to the switch.
  debouncer.update();
  if (debouncer.fell()) {                       // did we engage the switch?
    Serial.println ("fall");
    digitalWrite (PIN_ACTIVITY_B, LOW);         // turn off B LED
    source = 1;                                 // set source to MIDI A
    panic();                                    // send all notes off
    uart_flush(1);
  } else if (debouncer.rose()) {                // did we release the switch?
    Serial.println ("rise");
    digitalWrite (PIN_ACTIVITY_A, LOW);
    source = 2;                                 // set source to MIDI B
    panic();                                    // send all notes off
    uart_flush(2);
  }

  // write input bytes from the selected source to the MIDI output.
  if (source == 1) {                            // are we set to source A?
    if (Serial1.available()) {                  // yes. is there anything to read?
      MIDI_OUT.write (Serial1.read());          // yes. Write it straight to the output
      digitalWrite (PIN_ACTIVITY_A, LOW);       // turn off A LED
      offTime=millis() + LED_BLINK_TIME;        // set a timeout to turn it back on
    }
  } else if (source == 2) {                     // are we set to source B?
    if (Serial2.available()) {                  // ......
      MIDI_OUT.write (Serial2.read());
      digitalWrite (PIN_ACTIVITY_B, LOW);
      offTime=millis() + LED_BLINK_TIME;
    }
  }

  // handle LED blinky
  if (millis() > offTime) {                     // time to turn the LED back on again
    switch (source) {
      case 1: 
        digitalWrite (PIN_ACTIVITY_A, HIGH);
        break;
      case 2:
        digitalWrite (PIN_ACTIVITY_B, HIGH);
        break;
    
    }
  }
}


/*
 * send note off to all notes on all channels (this is a logic/mainstage style panic).
 * This will overload the transmit buffer as its about 6k of data, and will block 
 * until it's all sent (about 2 seconds)
 */
void panic() {
  uint32_t start=millis();
  for (uint8_t note=0; note<=127; note++) {
    for (uint8_t channel=0; channel<=15; channel++) {
      noteOff (channel, note);
    }
  }
  Serial.printf ("panic took %i ms\n", millis()-start);
}


/*
 * send a "proper" note off message (status 0x80)
 */
void noteOff (uint8_t channel, uint8_t note) {
  MIDI_OUT.write (0x80 + (channel & 0x0F));
  MIDI_OUT.write (note);
  MIDI_OUT.write (0x00);
}

/*
 * read the receive buffer until it's empty. We do this when switching inputs
 * so we don't retransmit anything that arrived before the switching command.
 */
void discardRX (HardwareSerial s) {
  uint8_t dummy;
  while (s.available() >0)
    dummy = s.read();
}
